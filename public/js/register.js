document.addEventListener('DOMContentLoaded', () => {
    let registerForm = document.querySelector('form.register-form');

    const postLoginData = (login, username, password) => {
        fetch('/register', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'user-email': login,
                'username': username,
                'user-password': MD5(password)
            })
        }).then(response => response.json()).then(data => {
            if (data.status == 200) {
                if (data.redirect) {
                    setCookie('authToken', data.token, {secure: true, 'max-age': 3600});
                    location.assign('/');
                }
            } else {
                alert('Some error occured!');
            }
        });
    }

    registerForm.addEventListener('submit', (e) => {
        e.preventDefault();
        let userEmail = document.querySelector('form.register-form input[name="user-email"]');
        let username = document.querySelector('form.register-form input[name="username"]');
        let userPassword = document.querySelector('form.register-form input[name="user-password"]');
        let userPasswordConfirmation = document.querySelector('form.register-form input[name="user-password-confirmation"]');
        if (userPassword.value == userPasswordConfirmation.value) {
            postLoginData(userEmail.value, username.value, userPassword.value);
        } else {
            alert('Password and password confirmation should be equial!');
        }
    });
});