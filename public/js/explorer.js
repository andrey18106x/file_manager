document.addEventListener("DOMContentLoaded", () => {
    let tableResponsiveWrapper = document.querySelector('.table-responsive');
    let explorerTable = document.querySelector(".explorer");
    let explorerTableBody = document.querySelector(".explorer tbody");
    let tableRows = document.querySelectorAll(".explorer tr[data-file]");
    let backButton = document.querySelector(".back-folder-button");
    let breadcrumbs = document.querySelector('.breadcrumbs');

    const loadEventListeners = () => {
        tableRows = document.querySelectorAll(".explorer tr[data-file]");
        explorerTable = document.querySelector(".explorer");
        explorerTableBody = document.querySelector(".explorer tbody");
        tableRows.forEach((row) => {
            if (row.dataset.directory === 'true') {
                row.addEventListener("dblclick", (e) => {
                    goToPath(row.dataset.file);
                });
            } else {
                row.addEventListener("click", (e) => {
                    getFileInfo(row);
                });
            }
        });
    }

    const goToPath = (path) => {
        fetch("/get_files?path=" + decodeURI(path), {
            method: "GET",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: { "Content-Type": "application/json", },
        }).then(res => res.text()).then(data => {
            tableResponsiveWrapper.innerHTML = data;
            breadcrumbs.innerHTML = decodeURI(path);
            window.history.pushState(
                {"html": document.html, "pageTitle": window.title}, 
                "row.dataset.file" + " - File Manager | Andrey Borysenko - GFL JS Course", 
                "?path=" + decodeURI(path)
            );
            clearFileInfo();
            loadEventListeners();
        });
    }

    backButton.addEventListener("click", () => {
        if (explorerTableBody.dataset.parent !== "" && new URLSearchParams(location.search).get("path") !== explorerTableBody.dataset.parent) {
            goToPath(explorerTableBody.dataset.parent);
        }
    });

    const imageTypes = ["png", "jpg", "jpeg", "webp"];
    const fileInfoImage = document.querySelector('.file-info-img img');

    const loadImageThumb = (pathToImage) => {
        let imageURL = `/static_explorer/${explorerTable.dataset.id}${pathToImage}`;
        fileInfoImage.setAttribute("src", imageURL);
    }

    const getFileInfo = (row) => {
        let fileInfoListItems = document.querySelectorAll('.file-info ul li');
        let tableRowItems = row.querySelectorAll('td');
        tableRowItems.forEach((tableCell, index, arr) => {
            if (index === 0) {
                fileInfoListItems[index].querySelector('span.value').innerHTML = tableCell.querySelector('span.file-name').innerHTML;
            } else if (index === 1) { // File Type column
                const fileType = tableCell.innerHTML;
                if (imageTypes.indexOf(fileType) !== -1) {
                    loadImageThumb(row.dataset.file);
                } else {
                    fileInfoImage.setAttribute("src", "/img/file.svg");
                }
                fileInfoListItems[index].querySelector('span.value').innerHTML = tableCell.innerHTML;
            } else {
                fileInfoListItems[index].querySelector('span.value').innerHTML = tableCell.innerHTML;
            }
        });
        let downloadButton = document.querySelector('.file-info .download-button');
        downloadButton.setAttribute('href', '/download?path=' + row.dataset.file);
    }

    const clearFileInfo = () => {
        let fileInfoListItems = document.querySelectorAll('.file-info ul li');
        fileInfoImage.setAttribute("src", "/img/file.svg");
        let downloadButton = document.querySelector('.file-info .download-button');
        downloadButton.setAttribute('href', "#");
        fileInfoListItems.forEach(item => {
            item.querySelector('span.value').innerHTML = "";
        })
    }

    loadEventListeners();

    // Handling Create folder

    let createFolderButton = document.querySelector(".create-folder-button");
    let createFolderPopup = document.querySelector(".folder-popup");
    let folderPopupBlackout = document.querySelector(".folder-popup-blackout");
    let newFolderNameInput = document.getElementById("new-folder-name");
    let createNewFolderBtn = document.querySelector(".create-new-folder-btn");
    let cancelCreateNewFolderBtn = document.querySelector('.cancel-new-folder-btn');

    createFolderButton.addEventListener("click", (e) => {
        e.preventDefault();
        createFolderPopup.classList.add("popup-active");
        folderPopupBlackout.classList.add("blackout-active");
    });

    const closeNewFolderPopup = () => {
        createFolderPopup.classList.remove("popup-active");
        folderPopupBlackout.classList.remove("blackout-active");
        newFolderNameInput.value = "";
    }

    folderPopupBlackout.addEventListener("click", closeNewFolderPopup);
    cancelCreateNewFolderBtn.addEventListener("click", closeNewFolderPopup);

    createNewFolderBtn.addEventListener("click", (e) => {
        e.preventDefault();
        fetch("/new_folder", {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: { "Content-Type": "application/json", },
            body: JSON.stringify({ 
                'newFolderName': newFolderNameInput.value,
                'currentPath': encodeURI(new URLSearchParams(location.search).get("path"))
            })
        }).then(res => res.json()).then(data => {
            if (data.status) {
                closeNewFolderPopup();
                goToPath(new URLSearchParams(location.search).get("path"));
            } else {
                alert("Some error occured, while creating a new folder");
            }
        });
    });

    // Handling Upload file

    let uploadFileButton = document.querySelector(".upload-file-button");
    let uploadFilePopup = document.querySelector(".upload-popup");
    let uploadPopupBlackout = document.querySelector(".upload-popup-blackout");
    let uploadFileInput = document.getElementById("upload-file");
    let uploadNewFileBtn = document.querySelector(".upload-file-btn");
    let cancelUploadNewFileBtn = document.querySelector('.cancel-upload-file-btn');

    uploadFileButton.addEventListener("click", (e) => {
        e.preventDefault();
        uploadFilePopup.classList.add("popup-active");
        uploadPopupBlackout.classList.add("blackout-active");
    });

    const closeFileUpload = (e) => {
        e?.preventDefault();
        uploadFilePopup.classList.remove("popup-active");
        uploadPopupBlackout.classList.remove("blackout-active");
        uploadFileInput.value = null;
    }

    uploadPopupBlackout.addEventListener("click", closeFileUpload);
    cancelUploadNewFileBtn.addEventListener("click", closeFileUpload);

    uploadNewFileBtn.addEventListener("click", (e) => {
        e.preventDefault();
        if (uploadFileInput.files.length === 1) {
            let formData = new FormData();
            formData.append('file', uploadFileInput.files[0]);
            formData.append('uploadTo', encodeURI(new URLSearchParams(location.search).get("path")));
            fetch("/upload", {
                method: "POST",
                mode: "cors",
                cache: "no-cache",
                credentials: "same-origin",
                body: formData
            }).then(res => res.json()).then(data => {
                if (data.success) {
                    closeFileUpload();
                    goToPath(new URLSearchParams(location.search).get("path"));
                    loadEventListeners();
                    let emptySpan = document.querySelector('.empty');
                    let explorerSizeSpan = document.querySelector('.explorer-size');
                    emptySpan.innerHTML = data.empty;
                    explorerSizeSpan.innerHTML = data.explorerSize;
                } else {
                    alert("Some error occured, while uploading file");
                }
            });
        } else {
            alert("Select one file to upload");
        }
    });

    // Handling the search

    let searchInput = document.getElementById('explorer-search');

    searchInput.addEventListener("input", () => {
        if (searchInput.value.length === 0) {
            tableRows.forEach((row) => {
                row.classList.remove("filtered");
            });
        }

        if (searchInput.value.length >= 3) {
            tableRows.forEach((row) => {
                let tableRowItems = row.querySelectorAll('td');
                if (row.dataset.directory === "true") {
                    let directoryName = tableRowItems[0].querySelector('span.directory-name').innerHTML.toString();
                    if (!directoryName.toLowerCase().includes(searchInput.value.toLowerCase())) {
                        row.classList.add("filtered");
                    } else {
                        row.classList.remove("filtered");
                    }
                } else {
                    let fileName = tableRowItems[0].querySelector('span.file-name').innerHTML.toString();
                    if (!fileName.toLowerCase().includes(searchInput.value.toLowerCase())) {
                        row.classList.add("filtered");
                    } else {
                        row.classList.remove("filtered");
                    }
                }
            });
        }
    });
});
