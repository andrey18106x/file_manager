document.addEventListener('DOMContentLoaded', () => {
    let loginForm = document.querySelector('form.login-form');

    const postLoginData = (login, password) => {
        fetch('/login', {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'user-email': login,
                'user-password': MD5(password)
            })
        }).then(response => response.json()).then(data => {
            if (data.status == 200) {
                if (data.redirect) {
                    console.log();
                    setCookie('authToken', data.token, {secure: true, 'max-age': 3600});
                    location.assign('/');
                }
            } else {
                alert('Email or password is invalid!');
            }
        });
    }

    loginForm.addEventListener('submit', (e) => {
        e.preventDefault();
        let userEmail = document.querySelector('form.login-form input[name="user-email"]');
        let userPassword = document.querySelector('form.login-form input[name="user-password"]');
        postLoginData(userEmail.value, userPassword.value);
    });
});