const express = require("express");
const path = require("path");
const hbs = require("hbs");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const fileUpload = require("express-fileupload");

const { homeRouter } = require("./routes");
const { UserController, FilesController } = require("./controllers");


const HOST = process.env.HOST || "localhost";
const PORT = process.env.PORT || 3000;
const PUBLIC = path.join(__dirname, "public");
const VIEWS = path.join(PUBLIC, "views");
const PARTIALS = path.join(PUBLIC, "views", "partials");
const EXPLORER = path.join(__dirname, "explorer");

const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(fileUpload({ createParentPath: true }));
app.use(cookieParser());
app.use(express.static(PUBLIC));
app.set("views", VIEWS);
app.set("view engine", "hbs");
hbs.registerPartials(PARTIALS);


app.get("/", homeRouter);


app.use("/register", UserController.register);
app.use("/login", UserController.login);
app.get("/logout", UserController.logout);


app.use("/explorer", FilesController.getPathFiles);
app.use("/static_explorer", express.static(EXPLORER));
app.use("/get_files", FilesController.getPathFilesApi);
app.use("/new_folder", FilesController.createFolder);
app.use("/download", FilesController.downloadFile);
app.use("/upload", FilesController.uploadFile);


app.use((req, res) => {
    res.render("pages/404.hbs", {
        title: "404 - File Manager | Andrey Borysenko - GFL JS Course",
    });
});


app.listen(PORT, () => {
    console.info(`Server is running at http://${HOST}:${PORT}`);
});
