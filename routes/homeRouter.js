const homeRouter = require("express").Router();
const UserModel = require('../models/UserModel');

homeRouter.get("/", (req, res) => {
    let token = "authToken" in req.cookies ? req.cookies["authToken"] : "";
    UserModel.getAuthorizedUser(token, (userId, username) => {
        let authorized = userId !== "-1" && username !== "" && userId !== undefined && username !== undefined;
        res.render('pages/home.hbs', {
            isAuthorized: 'authToken' in req.cookies && authorized,
            username: username,
            title: "File Manager | Andrey Borysenko - GFL JS Course"
        });
    });
});

module.exports = homeRouter;
