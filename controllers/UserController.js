const UserModel = require("../models/UserModel");
const md5 = require("md5");
const FilesController = require("./FilesController");

class UserController {
    isExists(login, callback) {
        UserModel.isExists(login, (result) => {
            callback(result);
        });
    }

    login(req, res) {
        let authToken = "authToken" in req.cookies ? req.cookies["authToken"] : "";
        if (req.method === "GET") {
            UserModel.getAuthorizedUser(authToken, (userId, username) => {
                let authorized = userId !== "-1" && username !== "" && userId !== undefined && username !== undefined;
                res.render("pages/login.hbs", {
                    isAuthorized: authToken !== "" && authorized,
                    title: "Login - File Manager | Andrey Borysenko - GFL JS Course",
                });
            });
            return;
        }

        let userEmail = req.body["user-email"];
        let userPassword = req.body["user-password"];

        UserModel.isValid(userEmail, userPassword, (result) => {
            if (result) {
                let token = md5(new Date().toString());
                UserModel.setToken(userEmail, token, (tokenCreated) => {
                    if (tokenCreated) {
                        res.send({
                            status: 200,
                            redirect: true,
                            token: token,
                        });
                    } else {
                        res.send({ status: 401, });
                    }
                });
            } else {
                res.send({ status: 401, });
            }
        });
    }

    register(req, res) {
        if (req.method === "GET") {
            res.render("pages/register.hbs", {
                isAuthorized: "authToken" in req.cookies,
                title: "Register - File Manager | Andrey Borysenko - GFL JS Course",
            });
            return;
        }

        let userEmail = req.body["user-email"];
        let username = req.body["username"];
        let userPassword = req.body["user-password"];

        UserModel.register(userEmail, username, userPassword, (result) => {
            if (result.success) {
                FilesController.createUserDirectory(result.msg.insertId.toString(), (directoryCreated) => {
                    if (directoryCreated) {
                        let token = md5(new Date().toString());
                        UserModel.setToken(req.body["user-email"], token, (tokenCreated) => {
                            if (tokenCreated) {
                                res.send({
                                    status: 200,
                                    redirect: true,
                                    token: token,
                                });
                            } else {
                                res.send({ status: 401, });
                            }
                        });
                    }
                });
            } else {
                res.send({ status: 401 });
            }
        });
    }

    logout(req, res) {
        let authToken = "authToken" in req.cookies ? req.cookies["authToken"] : "";
        if (authToken !== "") {
            UserModel.revokeToken(authToken, () => {
                res.sendStatus(200);
            });
        } else {
            res.sendStatus(401);
        }
    }
}

module.exports = new UserController();
