const FilesModel = require("../models/FilesModel");
const UserModel = require("../models/UserModel");
const filesize = require('filesize');

class FilesController {
    getPathFiles(req, res) {
        if (req.method === "GET" && "path" in req.query) {
            let authToken = "authToken" in req.cookies ? req.cookies["authToken"] : "";
            UserModel.getAuthorizedUser(authToken, (userId, username) => {
                if (userId !== "-1" && username !== "" && userId !== undefined && username !== undefined) {
                    FilesModel.getFile(userId, req.query.path, (file, parentPath) => {
                        let title = file.fileName 
                        ? file.fileName + " - File Manager | Andrey Borysenko  - GFL JS Course" 
                        : "File Manager | Andrey Borysenko  - GFL JS Course";
                        FilesModel.getUserDirectorySize(userId, (directorySize) => {
                            res.render("pages/explorer.hbs", {
                                file: file,
                                parentPath: parentPath,
                                isAuthorized: "authToken" in req.cookies,
                                username: username,
                                userId: userId,
                                title: title,
                                empty: filesize(process.env.EXPLORER_DEFAULT_CAPACITY - directorySize),
                                explorerSize: filesize(process.env.EXPLORER_DEFAULT_CAPACITY)
                            });
                        });
                    });
                } else {
                    res.render("pages/session_expired.hbs", {
                        title: "Session expired - File Manager | Andrey Borysenko - GFL JS Course",
                        redirectTo: req.query
                    });
                }
            });
        } else {
            res.redirect("/explorer?path=/");
        }
    }

    getPathFilesApi(req, res) {
        if (req.method === "GET" && "path" in req.query) {
            let authToken = "authToken" in req.cookies ? req.cookies["authToken"] : "";
            UserModel.getAuthorizedUser(authToken, (userId, username) => {
                if (userId !== "-1" && username !== "" && userId !== undefined && username !== undefined) {
                    FilesModel.getFile(userId, req.query.path, (file, parentPath) => {
                        FilesModel.getUserDirectorySize(userId, (directorySize) => {
                            res.render("partials/explorer_table.hbs", {
                                file: file,
                                parentPath: parentPath,
                                userId: userId,
                                username: username,
                                empty: filesize(process.env.EXPLORER_DEFAULT_CAPACITY - directorySize),
                                explorerSize: filesize(process.env.EXPLORER_DEFAULT_CAPACITY),
                            });
                        });
                    });
                } else {
                    res.render("pages/session_expired.hbs", {
                        title: "Session expired - File Manager | Andrey Borysenko - GFL JS Course",
                        redirectTo: req.query
                    });
                }
            });
        }
    }

    downloadFile(req, res) {
        let authToken = "authToken" in req.cookies ? req.cookies["authToken"] : "";
        UserModel.getAuthorizedUser(authToken, (userId, username) => {
            let authorized = userId !== "-1" && username !== "" && userId !== undefined && username !== undefined;
            if (!authorized) {
                res.render("pages/session_expired.hbs", {
                    title: "Session expired - File Manager | Andrey Borysenko - GFL JS Course",
                    redirectTo: req.query
                });
                return;
            }
            FilesModel.download(userId, req.query.path, (fileExists, pathToFile) => {
                if (fileExists) {
                    res.download(pathToFile);
                } else {
                    res.render('pages/download.hbs', {
                        isAuthorized: 'authToken' in req.cookies,
                        file: req.query.path
                    });
                }
            });
        });
    }

    uploadFile(req, res) {
        if (req.method === "POST" && "uploadTo" in req.body) {
            let token = "authToken" in req.cookies ? req.cookies["authToken"] : "";
            let uploadTo = decodeURI(req.body.uploadTo);
            UserModel.getAuthorizedUser(token, (userId, username) => {
                FilesModel.upload(req.files.file, userId, uploadTo, (fileUploaded, empty, explorerSize) => {
                    res.send({ 
                        success: fileUploaded, 
                        empty: filesize(empty), 
                        explorerSize: filesize(explorerSize) 
                    });
                });
            });
        }
    }

    createFolder(req, res) {
        if (req.method === "POST") {
            let authToken = "authToken" in req.cookies ? req.cookies["authToken"] : "";
            UserModel.getAuthorizedUser(authToken, (userId, username) => {
                if (userId !== "-1" && username !== "" && userId !== undefined && username !== undefined) {
                    let currentPath = decodeURI(req.body.currentPath);
                    let newFolderName = req.body.newFolderName;
                    FilesModel.createDirectory(userId, currentPath, newFolderName, (created) => {
                        if (created) {
                            res.send({ status: created });
                        }
                    });
                }
            });
        }
    }

    createUserDirectory(userId, callback) {
        FilesModel.createUserDirectory(userId, (result) => {
            callback(result);
        });
    }
}

module.exports = new FilesController();
