const UserController = require('./UserController');
const FilesController = require('./FilesController');

module.exports = {
    UserController,
    FilesController
}