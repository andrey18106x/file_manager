const Database = require("./database");

class UserModel {
  isExists(login, callback) {
    Database.executeQuery(
      "SELECT COUNT(`login`) AS `isExists` FROM `users` WHERE `login`=?",
      [login],
      (results) => callback(results)
    );
  }

  getUserId(login, callback) {
    Database.executeQuery(
      "SELECT user_id AS UserIdFROM `users` WHERE `login`=?",
      [login],
      (results) => callback(results.msg[0].UserId)
    );
  }

  isValid(login, password, callback) {
    this.isExists(login, (result) => {
      if (result.msg[0].isExists) {
        Database.executeQuery(
          "SELECT `login` AS `email`, `password` AS `password` FROM `users` WHERE `login`=? AND `password`=?",
          [login, password],
          (results) => {
            if (results.msg.length > 0) {
              callback(
                results.msg[0].email == login &&
                  results.msg[0].password == password
              );
            } else {
              callback(false);
            }
          }
        );
      } else {
        return false;
      }
    });
  }

  setToken(login, token, callback) {
    this.isExists(login, (result) => {
      if (result.msg[0].isExists) {
        Database.executeQuery("UPDATE `users` SET `token`=? WHERE `login`=?", [token, login], (results) => {
          callback(results.success);
        });
      } else {
        callback(false);
      }
    });
  }

  register(login, username, password, callback) {
    this.isExists(login, (existsResult) => {
      if (!existsResult.msg[0].isExists) {
        let currentDate = new Date();
        let currentDateString = `${currentDate.getFullYear()}-${currentDate.getMonth()}-${currentDate.getDay()}`;
        Database.executeQuery(
          "INSERT INTO `users`(login, username, password, date_added) VALUES (?, ?, ?, ?)",
          [login, username, password, currentDateString],
          (results) => {
            callback(results);
          }
        );
      } else {
        callback({success: false});
      }
    });
  }

  getAuthorizedUsername(token, callback) {
    if (token == "") {
      callback("");
    }
    Database.executeQuery(
      "SELECT `username` AS Username FROM `users` WHERE token=?",
      [token],
      (results) => {
        if (results.msg.length > 0) {
          callback(results.msg[0].Username);
        }
      }
    );
  }

  getAuthorizedUser(token, callback) {
    if (token == "") {
      callback();
    } else {
      Database.executeQuery(
        "SELECT `user_id` AS UserId, `username` AS Username FROM `users` WHERE token=?",
        [token],
        (results) => {
          if (results.msg.length > 0) {
            callback(results.msg[0].UserId.toString(), results.msg[0].Username);
          } else {
            callback("-1", "");
          }
        }
      );
    }
  }

  revokeToken(token, callback) {
    Database.executeQuery(
      "UPDATE `users` SET `token`=NULL WHERE `token`=?",
      [token],
      (result) => {
        callback();
      }
    );
  }
}

module.exports = new UserModel();
