const { FilesModel } = require('./FilesModel');
const { UserModel } = require('./UserModel');

module.exports = {
    FilesModel,
    UserModel
}