const path = require("path");
const fs = require("fs");
const filesize = require("filesize");
const fastFolderSize = require('fast-folder-size');

class FilesModel {
    constructor() {
        this.explorerRootDir = path.resolve(__dirname, "../explorer/");
    }

    getDirectoryFiles(explorerPath, userId, callback) {
        let infoFiles = [];
        fs.readdir(explorerPath, (err, files) => {
            if (err) throw err;
            files.forEach((file) => {
                let filePath = path.join(explorerPath, file);
                fs.stat(filePath, (fileError, stats) => {
                    if (fileError) throw fileError;
                    let fileInfo = {
                        isFile: stats.isFile(),
                        isDirectory: stats.isDirectory(),
                        size: filesize(stats.size),
                        created: stats.birthtime.toLocaleDateString(),
                        path: filePath,
                        fileName: path.basename(filePath).replace(path.extname(filePath), ""),
                        explorerPathUrl: encodeURI(filePath.substr(filePath.search("explorer") + "explorer".length + 1 + userId.length, filePath.length).split("\\").join("/")),
                        type: path.extname(filePath).replace(".", ""),
                    };
                    infoFiles.push(fileInfo);
                });
            });
            infoFiles.sort();
            callback(infoFiles);
        });
    }

    getFile(userId, filePath, callback) {
        let explorerFilePath = path.join(this.explorerRootDir, userId, filePath);
        fs.stat(explorerFilePath, (fileError, stats) => {
            if (fileError) throw fileError;
            let parentPath = path.dirname(explorerFilePath);
            parentPath = encodeURI(parentPath.substr(parentPath.search("explorer") + "explorer".length + + 1 + userId.length, parentPath.length).split("\\").join("/") + "/");
            let fileInfo = {
                isFile: stats.isFile(),
                isDirectory: stats.isDirectory(),
                size: stats.size,
                created: stats.birthtime.toLocaleDateString(),
                path: filePath,
                fileName: path.basename(filePath).replace(path.extname(filePath), ""),
                type: path.extname(filePath).replace(".", ""),
            };
            if (stats.isDirectory()) {
                this.getDirectoryFiles(explorerFilePath, userId, (files) => {
                    fileInfo.files = files;
                    callback(fileInfo, parentPath);
                });
            } else if (stats.isFile()) {
                callback(fileInfo);
            }
        });
    }

    createDirectory(userId, currentDirectory, newDirectoryName, callback) {
        let newDirectory = path.join(this.explorerRootDir, userId, currentDirectory, newDirectoryName);
        if (!fs.existsSync(newDirectory)) {
            fs.mkdirSync(newDirectory);
            callback(true);
        } else {
            callback(false);
        }
    }

    createUserDirectory(userId, callback) {
        let newExplorerDirectory = path.resolve(this.explorerRootDir, userId);
        if (!fs.existsSync(newExplorerDirectory)) {
            fs.mkdirSync(newExplorerDirectory);
            callback(true);
        } else {
            callback(false);
        }
    }

    getUserDirectorySize(userId, callback) {
        let userExplorerPath = path.join(this.explorerRootDir, userId);
        fastFolderSize(userExplorerPath, (err, bytes) => {
            if (err) throw err;
            callback(bytes);
        });
    }

    download(userId, pathToFile, callback) {
        let localPathToFile = path.join(this.explorerRootDir, userId, decodeURI(pathToFile));
        fs.stat(localPathToFile, (err, stats) => {
            if (!err) {
                callback(true, localPathToFile);
            } else {
                callback(false, localPathToFile);
            }
        })
    }

    upload(file, userId, destination, callback) {
        let newFilePath = path.join(this.explorerRootDir, userId, destination, file.name);
        if (!fs.existsSync(newFilePath)) {
            this.getUserDirectorySize(userId, (userDirectorySize) => {
                let newDirectorySize = userDirectorySize + file.size;
                let empty = process.env.EXPLORER_DEFAULT_CAPACITY - newDirectorySize;
                let explorerSize = process.env.EXPLORER_DEFAULT_CAPACITY;
                if (newDirectorySize <= process.env.EXPLORER_DEFAULT_CAPACITY) {
                    file.mv(newFilePath);
                    callback(true, empty, explorerSize);
                } else {
                    callback(false, empty, explorerSize);
                }
            });
        } else {
            callback(false);
        }
    }
}

module.exports = new FilesModel();
