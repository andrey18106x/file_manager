const mysql = require("mysql2");

class Database {
    constructor() {
        this.conn = mysql.createPool({
            connectionLimit: 10,
            host: process.env.DATABASE_HOST,
            port: process.env.DATABASE_PORT,
            user: process.env.DATABASE_USER,
            password: process.env.DATABASE_PASSWORD,
            database: process.env.DATABASE_NAME,
        });
    }

    executeQuery(...args) {
        let query = args[0];
        let params = null;
        let callback = null;

        if (args.length === 2) {
            callback = args[1];
            params = [];
        } else if (args.length === 3) {
            params = args[1];
            callback = args[2];
        } else {
            throw new Error("Invalid arguments count");
        }

        this.conn.execute(query, params, (error, result) => {
            if (error) {
                callback(this._error("Mysql query error\n" + error));
            } else {
                callback(this._success(result));
            }
        });
    }

    promise() {
        return this.con.promise();
    }

    _success(msg) {
        return {
            success: true,
            msg,
        };
    }

    _error(msg) {
        return {
            success: false,
            msg,
        };
    }

    closeConnection() {
        this.con.end();
    }
}

module.exports = new Database();
